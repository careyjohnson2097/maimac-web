$(document).ready(function() {
  $('#autoWidth').lightSlider({
      animateOut:"fadeOut",
      autoplay:true,
      autoWidth:true,
      loop:true,
      autoplayTimeout:3000,
      nav:false,
      items:1,
      onSliderLoad: function() {
          $('#autoWidth').removeClass('cs-hidden');
      } 
  });  
});