// navigation
const navOpen=document.querySelector(".nav_hamburger");
const navClose=document.querySelector(".close_toggle");
const menu=document.querySelector(".nav_menu");
const navContainer=document.querySelector(".nav_menu");

navOpen.addEventListener('click',()=>{
  menu.classList.add("open");
  document.body.classList.add("active");
  navContainer.style.left="0";
  navContainer.style.width="17rem";
});
navClose.addEventListener('click',()=>{
  menu.classList.remove("open");
  document.body.classList.remove("active");
  navContainer.style.removeProperty("left");
  navContainer.style.removeProperty("width");
});

(function ($){
  "use strict";
  new WOW().init();

  //sticky navbar
  $(window).on("scroll", function () {
    var scroll = $(window).scrollTop();
    if (scroll < 100) {
      $(".sticky-header").removeClass("sticky");
    } else {
      $(".sticky-header").addClass("sticky");
    }
  });

  //background image
  function dataBackgroundImage(){
    $("[data-bgimg]").each(function(){
      var bgImgUrl = $(this).data("bgimg");
      $(this).css({
        "background-image":"url("+bgImgUrl+")", //concatenation
      });
    });
  }

  $(window).on("load",function(){
    dataBackgroundImage();
  });

  //for carousel slider of the slider section
  $(".slider_area").owlCarousel({
    animateOut:"fadeOut",
    autoplay:true,
    loop:true,
    nav:false,
    autoplayTimeout:5000,
    items:1,
    dots:true,
  });

  //product column responsive
  $(".product_column3").slick({
    centerMode: true,
    centerPadding: "0",
    slidesToShow: 4,
    arrows: true,
    rows: 2,
    prevArrow:
      '<button class="prev_arrow"><i class="ion-chevron-left" ></i></button><i class="ion-chevron-left"></i>',
    nextArrow:
      '<button class="next_arrow"><i class="ion-chevron-right"></i></button><i class="ion-chevron-right"></i>',
    responsive: [
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 996,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        },
      },
      // {
      //   breakpoint: 1024,
      //   settings: {
      //     slidesToShow: 4,
      //     slidesToScroll: 4,
      //   },
      // },
    ],
  });

  //for tooltip
  $('[data-toggle="tooltip"]').tooltip();

  //tooltip active
  $(".action_links ul li a, .quick_button a").tooltip({
    animated: "fade",
    placement: "top",
    container: "body",
  });

  //navactive responsive
  $(".product_navactive").owlCarousel({
    autoplay: false,
    loop: true,
    nav: true,
    items: 4,
    dots: false,
    navText: [
      '<i class="ion-chevron-left arrow-left"></i>',
      '<i class="ion-chevron-right arrow-right"></i>',
    ],
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
      },
      250: {
        items: 2,
      },
      480: {
        items: 3,
      },
      768: {
        items: 4,
      },
    },
  });

  $(".modal").on("shown.bs.modal", function (e) {
    $(".product_navactive").resize();
  });

  $(".product_navactive a").on("click", function (e) {
    e.preventDefault();
    var $href = $(this).attr("href");
    $(".product_navactive a").removeClass("active");
    $(this).addClass("active");
    $(".product-details-large .tab-pane").removeClass("active show");
    $(".product-details-large " + $href).addClass("active show");
  });
  
})(jQuery);


AOS.init();